<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
$u = new Utilizadores();
  
 
  // detectar se passou o inserir
  if (isset($_POST["btnInserir"])){
    // chama o metodo para inserir o utilizador
    $_POST["id_tipo_utilizador"] = 2;
    $t = $_POST["id_tipo_utilizador"];
    $util = $_POST["utilizador"];
    $p = $_POST["palavrapasse"];
    $e = $_POST["email"];
    $u->inserirNovoUtilizador($t,$util,$p,$n,$e);
  }
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
   
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Utilizadores \ 
          <small class="badge badge-dark menuativo">CRIAR CONTA</small></h1>
          <hr>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <form action="user_new2.php" method="post" onsubmit="return validaUtilizadorNovo();">
            
            
            <div class="form-group">
              <label for="utilizador">Utilizador</label>
              <input type="text" id="utilizador" name="utilizador" class="form-control">
            </div>

            <div class="form-group">
              <label for="palavrapasse">Digite a Palavra Passe</label>
              <input type="password" id="palavrapasse" name="palavrapasse" class="form-control">
            </div>

            <div class="form-group">
              <label for="palavrapasse2">Confirme a Palavra Passe</label>
              <input type="password" id="palavrapasse2" name="palavrapasse2" class="form-control">
            </div>

            <div class="form-group">
              <label for="email">Email do Utilizador</label>
              <input type="email" id="email" name="email" class="form-control">
            </div>  

            <hr>
            <button type="submit" class="btn btn-success" name="btnInserir" value="inserir">INSERIR</button>          <button type="reset" class="btn btn-danger">LIMPAR</button>             <hr>              

          </form>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>