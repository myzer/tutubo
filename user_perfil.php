<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
  // vem da session
  $dadosatuais = $u->utilizadorPorId($_SESSION["id"]);
  // detectar se passou o inserir
  if (isset($_POST["btnEditar"])){
    // editar o utilizador
    // chamar o método para editar
    $u->editarUtilizador($_POST);
  }
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>EDITAR PERFIL</h1>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <form action="user_perfil.php" method="post" onsubmit="return validaUtilizadorEditar();">
            <div class="form-group">
              <label for="id_tipo_utilizador">Defina o tipo de utilizador</label>
              <select id="id_tipo_utilizador2" name="id_tipo_utilizador2" class="form-control" disabled>
                <option value="0">--- Escolha um tipo</option>
                <?php 
                  $tipos = $u->listaTiposUtilizadores();
                  foreach ($tipos as $tipo){
                ?>
                  <option value="<?php echo $tipo["id_tipo_utilizador"]; ?>" <?php 
                  if ($tipo["id_tipo_utilizador"]==$dadosatuais["id_tipo_utilizador"]){echo " selected";}
                  ?>><?php echo $tipo["nome_tipo"]; ?></option>
                <?php } ?>
              </select>
              <input type="hidden" name="id_tipo_utilizador" value="<?php echo $tipo["id_tipo_utilizador"]; ?>">
            </div>
            
            <div class="form-group">
              <label for="utilizador">Utilizador</label>
              <input type="text" id="utilizador" name="utilizador" class="form-control" value="<?php echo $dadosatuais['utilizador'];?>" readonly>
            </div>
            <div class="form-group">
              <label for="nome">Nome do Utilizador</label>
              <input type="text" id="nome" name="nome" class="form-control" value="<?php echo $dadosatuais['utilizador'];?>">
            </div> 

            <div class="form-group">
              <label for="email">Email do Utilizador</label>
              <input type="email" id="email" name="email" class="form-control" value="<?php echo $dadosatuais['email'];?>">
            </div>

            <div class="form-group">
              <label for="palavrapasse">Digite a Palavra Passe</label>
              <input type="password" id="palavrapasse" name="palavrapasse" class="form-control" >
            </div>

            <div class="form-group">
              <label for="palavrapasse2">Confirme a Palavra Passe</label>
              <input type="password" id="palavrapasse2" name="palavrapasse2" class="form-control">
            </div>

              

            <div class="form-group">
            <input type="checkbox" id="ativo2" name="ativo2" <?php 
            if ($dadosatuais['ativo']){echo " checked";}
            ?> disabled> <label for="ativo"> Utilizador Ativo</label>
            <input type="hidden" name="ativo" value="<?php 
            if ($dadosatuais['ativo']){echo "on";}
            ?>"> 
            </div>  

            <hr>
            <input type="hidden" name="id_utilizador" value="<?php echo $_SESSION["id"];?>">
            <button type="submit" class="btn btn-success" name="btnEditar" value="editar">EDITAR</button>          <button type="reset" class="btn btn-danger">LIMPAR</button>             <hr>              

          </form>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>