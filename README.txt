-----------------------Cuidados a ter antes de utilizar----------------
Para poder utilizar correctamente a funcionalidade de upload de videos �
necess�rio alterar os seguintes parametros no ficheiro php.ini:



Tamanho maximo autorizado pelo php para upload de ficheiros

upload_max_filesize = 200M

Valor que tem que ser maior ou igual que o upload_max_filesize

post_max_size = 200M

------------------------------------//--------------------------------

NOTA: O projeto s� leva 1 video uploaded para n�o exceder o limite 
de upload do MOODLE.




------------------------- FUNCIONALIDADES -----------------------------

FrontOffice

- Pesquisar por videos via search bar.
- Pesquisa de videos por categoria
- Listar os ultimos 3 videos (apenas 1 inserido por defeito por causa das limita��es do moodle).
- Login (ou aceder ao Painel de controlo/Backoffice)
	* No login temos a fun��o de registar utilizadores, no entanto difere com a vers�o do backoffice visto que esta
	  s� permite acrescentar utilizadores normais. Depois de registar volta para o login.
- S� ser�o apresentadas as thumbnails do video. Para ver o video basta clicar na imagem e ir� ser redirecionado para a pagina do v�deo.

- Junto de cada video aparecem dois badges. Um com a categoria e outro com o uploader.


BackOffice

- Se estivermos logados como admin, temos acesso � gest�o de todos os v�deos que foram uploaded, assim como 
  podemos inserir utilizadores novos do tipo administrador, assim como do tipo normal.

- Se estivermos logados com uma conta de utilizador normal, apenas podemos gerir os videos que esse utilizador deu upload (update e delete)

- Podemos inserir categorias novas caso estejamos logados com uma conta admin assim como inserir videos normalmente

- Para inserir videos, s�o necess�rios fazer upload de dois ficheiros. Um que serve como a thumbnail do video (jpg ou png)
  e o ficheiro de video em si (que s� pode ser em .mp4).



Utilizadores predefinidos

Administrador
 admin
 admin

Utilizador Normal
 MyzerHD
 123





