<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <div class="jumbotron bg-danger">
            <h1>Painel de Controlo </h1>
            <small><?php echo NOMEWEBSITE . " v." . VERSAO; ?></small>
          </div>
        </div>
      </div>

      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>