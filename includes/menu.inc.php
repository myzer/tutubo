<?php require_once 'config.inc.php'; ?>
<!-- menu -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="index.php"><span class ="red"><?php echo NOMEWEBSITE; ?></span></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php"><i class="fas fa-home"></i>
Home</a></li>
          <!-- menu categorias -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             Categorias
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <?php 
              require_once 'classes/categorias.class.php';
              $c = new Categorias();
              $categorias = $c->listaCategorias();
              foreach($categorias as $categoria){
              ?>
              <a class="dropdown-item" href="categoria.php?cat=<?php echo $categoria["id_categoria"];?>"><?php echo $categoria["nomecat"];?></a>
              <?php } ?>
            </div>
          </li>
          <!-- fim menu do menu categorias -->
          <!-- acesso login / backoffice -->
          <?php 
          // faz check se existe session start
          if (session_status()==PHP_SESSION_NONE){
            session_start();
          }
          if (!isset($_SESSION["login"])) { ?>
          <li class="nav-item">
            <a class="nav-link" href="login.php"><i class="fas fa-user"></i> Login</a>
          </li>
          <?php } else {?>
          <li class="nav-item">
              <a class="nav-link" href="privado.php"><i class="fas fa-user"></i> Painel de Controlo</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle"></i> <?php echo $_SESSION["utilizador"]; ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="user_perfil.php"><i class="fas fa-user-edit"></i> Editar Perfil</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.php"><i class="fas fa-sign-out-alt"></i> Terminar Sessão</a>
            </div>
          </li>
          <?php }?> 
          <!-- fim de acesso login / backoffice -->     
          <form action="pesquisa.php" id="pesquisar" class="form-inline my-2 my-lg-0" method="post">
                <input class="form-control mr-sm-2" type="search" placeholder="Procurar" name="pesquisa">
                <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Procurar</button>
          </form>
        </ul>
        
      </div>
    </nav>
    <!-- fim de menu -->