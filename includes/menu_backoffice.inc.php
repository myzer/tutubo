<?php require_once 'config.inc.php'; ?>
<!-- menu -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="index.php"><i class="fas fa-user-secret"></i> Backoffice <span class="badge badge-danger"><?php echo NOMEWEBSITE;?></span></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item ">
            <a class="nav-link" href="privado.php"><i class="fas fa-home"></i> Início </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="index.php"><i class="far fa-newspaper"></i> FrontOffice </a>
          </li>
          <!-- menu videos -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-book-open"></i> Videos
            </a> 
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="vid_new.php"><i class="fas fa-plus-circle"></i> Inserir Video</a>
              <a class="dropdown-item" href="vid_manager.php"><i class="fas fa-list-ul"></i> Gerir Videos</a>
              
              
              <?php 
              if (!in_array("cat_new.php",PERMCOLABORADOR) || $_SESSION["id"]==1 ){
              ?>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="cat_new.php"><i class="fas fa-folder-plus"></i> Inserir Categoria</a>
              <?php }?>
              <?php 
              if (!in_array("cat_manager.php",PERMCOLABORADOR) || $_SESSION["id"]==1) {
              ?>              
              <a class="dropdown-item" href="cat_manager.php"><i class="fas fa-list-ul"></i> Gerir Categorias</a>
              <?php }?>
            </div>
          </li>
          <!-- fim de menu videos -->
          
          <?php 
            if ($_SESSION["tipo"]==1){
          ?>
          <!-- menu utilizadores -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-users"></i> Utilizadores
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="user_new.php"><i class="fas fa-user-plus"></i> Inserir</a>
              <a class="dropdown-item" href="user_manager.php"><i class="fas fa-list-ul"></i> Gerir</a>
            </div>
          </li>
          <!-- fim de menu utilizadores -->
          <?php } ?>

          <!-- menu do proprio user -->
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle"></i> <?php echo $_SESSION["utilizador"]; ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="user_perfil.php"><i class="fas fa-user-edit"></i> Editar Perfil</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.php"><i class="fas fa-sign-out-alt"></i> Terminar Sessão</a>
            </div>
          </li>
          <!-- fim menu do proprio user -->
        </ul>
        
      </div>
    </nav>
    <!-- fim de menu -->