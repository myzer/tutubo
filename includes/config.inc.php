<?php
// ficheiro de configuração da aplicação
const NOMEWEBSITE   = "TuTubo";
const VERSAO        = "0.1";
// créditos
const CREDITOS = "&copy; Desenvolvido por : Márcio Sousa 2018";
// configurações para a base de dados
const BASEDADOS = [
    "servidor"  => "localhost",
    "nomebd"    => "tutubo",
    "user"      => "root",
    "password"  => "mysql"
];
// SALT
// salt + salt + salt + password + salt
$saltstring = md5(".../ISLA|2018!dwdm*DPM\...");
define("SALT",(string)$saltstring);
// definições da aplicação
// activar o detalhe do erro da base de dados
const DEVMODE = true;
const PERMCOLABORADOR = [
    'user_new.php',         //página inserir user
    'user_edit.php',        //página gerir user
    'user_manager.php',     //página editar user
    'cat_new.php',          //página inserir categoria
    'cat_manager.php',      //página gerir categoria
    'cat_edit.php'          //página editar categoria

];

?>