    <?php require_once "config.inc.php"; ?>
    
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="vendors/font-awesome_5.3/css/all.css">

    <!-- Website CSS -->
    <link rel="stylesheet" href="css/site.css">

    <title><?php echo NOMEWEBSITE ;?></title>