    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- SweetAlert 2.1 -->
    <script src="vendors/sweetalert_2.1/sweetalert.min.js"></script>
    <!-- JS Website -->
    <script src="js/app.js"></script>