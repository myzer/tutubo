<?php
  // se veio do formulário
  if (isset($_POST["btnEnviar"])){
    // ligação à classe dos utilizadores
    require_once 'classes/utilizadores.class.php';
    // instanciar a classe
    $u = new Utilizadores();
    // chamar o login
    $u->login($_POST["utilizador"],$_POST["palavrapasse"],"privado.php");
  }

  if (isset($_POST["btnReg"])){
    header("Location:user_new2.php");
  }
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head.inc.php';?>
  </head>
  <body>
    <?php require_once 'includes/menu.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <div class="jumbotron">
            <h1>LOGIN</h1>
            <small><?php echo NOMEWEBSITE . " v." . VERSAO; ?></small>
          </div>
        </div>
      </div>

      <!-- formulário de login -->
      <div class="row justify-content-center">
        <div class="col-12 col-md-4">
          <form action="login.php" method="post" onsubmit="return validaLogin();">
            <div class="form-group">
              <label for="utilizador">Utilizador</label>
              <input type="text" class="form-control" id="utilizador" name="utilizador"  placeholder="Colocar o Utilizador...">
            </div>
            <div class="form-group">
              <label for="palavrapasse">Palavra-Passe</label>
              <input type="password" class="form-control" id="palavrapasse" name="palavrapasse" placeholder="Coloque a sua palavra-passe...">
            </div>
            <button type="submit" class="btn btn-primary" name="btnEnviar" id="btnEnviar" value="entrar">Entrar <i class="fas fa-paper-plane"></i></button>
            <button type="submit" class="btn btn-danger" name="btnReg" id="btnReg" value="registar">Registar <i class="fas fa-lock"></i></button>
            

            <?php 
            // zona de erro
            if(isset($_GET["erro"]) && $_GET["erro"]=="acesso"){
              $erro = <<< EOT
              <hr><div class="alert alert-danger alert-dismissible fade show" role="alert">
              <strong>ERRO DE SEGURANÇA</strong> Para ter acesso à página tem que fazer um login válido
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
EOT;
              echo $erro;
            }
            
            ?>
          </form> 
          
        </div>    
      </div>
  
    </div>
  <?php require_once 'includes/scripts.inc.php';?>
  </body>
</html>