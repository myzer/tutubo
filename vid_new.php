<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure(__FILE__);
  // categorias de noticias
  require_once "classes/categorias.class.php";
  $c = new Categorias();
  $categorias = $c->listaCategorias(true);
  // detectar se passou o inserir
  if (isset($_POST["btnInserir"])){
    // metodo para inserir noticia
    require_once "classes/videos.class.php";
    $n = new Videos();
    $n->inserirVideo($_POST);
  }
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Vídeos \ 
          <small class="badge badge-dark menuativo">INSERIR NOVO VÍDEO</small></h1>
          <hr>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          
          <form action="vid_new.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="id_categoria">Escolha a categoria do vídeo</label>
              <select id="id_categoria" name="id_categoria" class="form-control">
                <option value="0">--- Escolha uma categoria</option>
                <?php 
                  foreach ($categorias as $categoria){
                ?>
                  <option value="<?php echo $categoria["id_categoria"]; ?>"><?php echo $categoria["nomecat"]; ?></option>
                <?php } ?>
              </select>
            </div>
            
            <div class="form-group">
              <label for="titulo">Título do Video</label>
              <input type="text" id="titulo" name="titulo" class="form-control">
            </div>

            <div class="form-group">
              <label for="descricao">Descrição do Vídeo</label>
              <textarea rows="10" cols="60" name="descricao" id="descricao" class="form-control"></textarea>
            </div>

            <div class="form-group">
              <label for="thumbnail">Thumbnail</label>
              <input type="file" id="imgupload" name="imgupload" class="form-control">
            </div> 

            <div class="form-group">
              <label for="video">Ficheiro de Video</label>
              <input type="file" id="vidupload" name="vidupload" class="form-control">
            </div> 

            <div class="form-group">
                <input type="checkbox" id="ativo" name="ativo"  checked> <label for="ativo">Video Ativo</label>
            </div>  

            <hr>
            <input type="hidden" id="thumbnail" name="thumbnail" value="imgupload">
            <input type="hidden" id="video" name="video" value="vidupload">
            <button type="submit" class="btn btn-success" name="btnInserir" value="inserir">INSERIR</button>          <button type="reset" class="btn btn-danger">LIMPAR</button>             <hr>              

          </form>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
   
  </body>
</html>