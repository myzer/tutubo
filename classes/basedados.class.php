<?php
// class responsável pela base de dados
require_once './includes/config.inc.php';

class Basedados {

    // variável que vai guardar a string de ligação à bd
    private $connection;

    // construtor da class
    public function __construct (PDO $connection = null){
        $this->connection = $connection;
        // testa se a variável $connection vem com dados de ligação
        if ($this->connection == null) {
            try{
                $this->connection = new PDO(
                    "mysql:host=" . BASEDADOS['servidor'] . 
                    ";dbname=" . BASEDADOS['nomebd'] , 
                    BASEDADOS['user'] ,BASEDADOS['password']
                    );
                // UTF8 para suportar caracteres especiais
                $this->connection->exec('SET NAMES "utf8"');
                if(DEVMODE){
                    // error reporting
                    $this->connection->setAttribute(
                    PDO::ATTR_ERRMODE, 
                    PDO::ERRMODE_EXCEPTION
                    );
                }
                
            }
                catch (PDOException $e){
                echo("Erro de ligação:" . $e);
                exit();    
            }
        }
    }

    // função para retornar o objeto pdo
    public function getCon(){
        return $this->connection;
    }
    
    

}



?>