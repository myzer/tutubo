<?php 
// classe responsável pelas categorias


class Categorias{


    // método lista categorias
    function listaCategorias($todas = false){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        if ($todas){
            $sql = "
            SELECT id_categoria,nomecat,ativo
            FROM CATEGORIAS "; 
        } else {
            $sql = "
            SELECT id_categoria,nomecat,ativo
            FROM CATEGORIAS 
            WHERE ativo = true ";
        }
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // executar query
        $dados->execute();
        // retornar valores
        return $resultado = $dados->fetchAll();
    }

    // método Inserir Categoria Nova
    function inserirCategoria($nomecat){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        INSERT INTO CATEGORIAS 
        (nomecat)
        VALUES (:n)
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // colocar os parametros
        $dados->bindValue(':n',$nomecat);
        // executar query
        $dados->execute();
        // reencaminhar
        header("Location:privado.php");
    }

    // método Lista de categoria por id
    function categoriaPorId($id){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
            SELECT id_categoria,nomecat,ativo
            FROM CATEGORIAS 
            WHERE id_categoria = :i
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        $dados->bindValue(':i',$id);
        // executar query
        $dados->execute();
        // retornar valores
        return $resultado = $dados->fetch();
    }

    // método Editar Categoria 
    function editarCategoria($cat){
        // chamar class basedados
        require_once "basedados.class.php";
        
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();    
        $sql = "
            UPDATE CATEGORIAS
            SET nomecat = :n, ativo = :a
            WHERE id_categoria = :i
        ";
        // trocar o checkbox = on
        if($cat["ativo"]=="on"){
            $cat["ativo"]=true;
        }else{
            $cat["ativo"]=false;
        }
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // colocar os parametros
        $dados->bindValue(':n',$cat["nomecat"]);
        $dados->bindValue(':a',$cat["ativo"]);
        $dados->bindValue(':i',$cat["id_categoria"]);
        // executar query
        $dados->execute();
        // reencaminhar
        header("Location:cat_manager.php");
    }

    // método apagar Categoria por id
    function apagarCategoria($id){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        DELETE FROM CATEGORIAS
        WHERE id_categoria = :i
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        $dados->bindValue(":i",$id);
        // executar query
        $dados->execute();
        // reencaminhar
        header("Location:cat_manager.php");
    }


// fim da classe
}

?>