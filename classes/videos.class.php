<?php 
// classe responsável pelos videos


class Videos{

    // método para mostrar as X últimos videos
    function ultimosVideos($num){
    // se número existe e é numérico 
    if(isset($num) && is_numeric($num)){
        // vai buscar os videos
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        SELECT VIDEOS.id_video, VIDEOS.id_categoria, VIDEOS.id_utilizador, CATEGORIAS.nomecat, UTILIZADORES.utilizador, VIDEOS.titulo, VIDEOS.descricao, VIDEOS.data, Videos.thumbnail, VIDEOS.video, VIDEOS.ativo
        FROM CATEGORIAS,VIDEOS,UTILIZADORES
        WHERE CATEGORIAS.id_categoria = VIDEOS.id_categoria AND VIDEOS.ativo = true AND CATEGORIAS.ativo = true AND UTILIZADORES.id_utilizador = VIDEOS.id_utilizador
        ORDER BY VIDEOS.data DESC
        LIMIT :l";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // paramêtros
        $dados->bindValue(":l",$num,PDO::PARAM_INT);
        // executar query
        $dados->execute();
        // retornar valores
        return $resultado = $dados->fetchAll();
        }
    }

    // método indicar a última Video
    function ultimoVideo(){
            // vai buscar os videos
            // chamar class basedados
            require_once "basedados.class.php";
            // instanciar a classe
            $pdo = new BaseDados();
            $pdo = $pdo->getCon();
            $sql = "
            SELECT VIDEOS.id_video, VIDEOS.id_categoria, VIDEOS.id_utilizador, CATEGORIAS.nomecat, VIDEOS.titulo, VIDEOS.descricao, VIDEOS.thumbnail, VIDEOS.data, VIDEOS.video, VIDEOS.ativo
            FROM CATEGORIAS, VIDEOS
            WHERE CATEGORIAS.id_categoria = VIDEOS.id_categoria 
            ORDER BY VIDEOS.id_video DESC
            LIMIT 1";
            // prepara a ligação ao sql
            $dados = $pdo->prepare($sql);
            // executar query
            $dados->execute();
            // retornar valores
            return $resultado = $dados->fetch();
        }

    // método para mostrar todas os videos por cat
    function videosPorCat($categoria){
        // se número existe e é numérico 
        if(isset($categoria) && is_numeric($categoria)){
            // vai buscar os videos
            // chamar class basedados
            require_once "basedados.class.php";
            // instanciar a classe
            $pdo = new BaseDados();
            $pdo = $pdo->getCon();
            $sql = "
            SELECT VIDEOS.id_video, VIDEOS.id_categoria, CATEGORIAS.nomecat, VIDEOS.titulo, VIDEOS.descricao, VIDEOS.data, VIDEOS.thumbnail, VIDEOS.video, VIDEOS.ativo, UTILIZADORES.utilizador
            FROM CATEGORIAS, VIDEOS, UTILIZADORES
            WHERE CATEGORIAS.id_categoria = VIDEOS.id_categoria AND VIDEOS.ativo = true AND UTILIZADORES.id_utilizador = VIDEOS.id_utilizador AND CATEGORIAS.ativo = true AND CATEGORIAS.id_categoria = :cat
            ORDER BY VIDEOS.data DESC
            ";
            // prepara a ligação ao sql
            $dados = $pdo->prepare($sql);
            // paramêtros
            $dados->bindValue(":cat",$categoria,PDO::PARAM_INT);
            // executar query
            $dados->execute();
            // retornar valores
            return $resultado = $dados->fetchAll();
            }
        }

    // método para pesquisar videos por string
    function pesquisarVideo($string){
        
            // vai buscar os videos
            // chamar class basedados
            require_once "basedados.class.php";
            // instanciar a classe
            $pdo = new BaseDados();
            $pdo = $pdo->getCon();
            $sql = "
            SELECT VIDEOS.id_video, VIDEOS.id_categoria, CATEGORIAS.nomecat, VIDEOS.titulo, VIDEOS.descricao, VIDEOS.data, VIDEOS.thumbnail, VIDEOS.video, VIDEOS.ativo
            FROM CATEGORIAS, VIDEOS
            WHERE CATEGORIAS.id_categoria = VIDEOS.id_categoria AND VIDEOS.ativo = true AND VIDEOS.ativo = true AND (VIDEOS.titulo like :p OR VIDEOS.descricao like :p)
            ORDER BY VIDEOS.data DESC
            ";
            // prepara a ligação ao sql
            $dados = $pdo->prepare($sql);
            // paramêtros
            $dados->bindValue(":p","%".$string."%");
            // executar query
            $dados->execute();
            // retornar valores
            return $resultado = $dados->fetchAll();
     
        }

    // método para mostrar noticia por ID
    function videosPorId($id){
    // se número existe e é numérico 
    if(isset($id) && is_numeric($id)){
        // vai buscar os videos
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        SELECT VIDEOS.id_video, VIDEOS.id_categoria, VIDEOS.id_utilizador, UTILIZADORES.utilizador, CATEGORIAS.nomecat, VIDEOS.titulo, VIDEOS.descricao, VIDEOS.data, VIDEOS.thumbnail, VIDEOS.video, VIDEOS.ativo
        FROM CATEGORIAS,VIDEOS,UTILIZADORES
        WHERE CATEGORIAS.id_categoria = VIDEOS.id_categoria AND VIDEOS.ativo = true AND VIDEOS.ativo = true AND VIDEOS.id_video = :i AND UTILIZADORES.id_utilizador=VIDEOS.id_utilizador
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // paramêtros
        $dados->bindValue(":i",$id,PDO::PARAM_INT);
        // executar query
        $dados->execute();
        // retornar valores
        return $resultado = $dados->fetch();
        }
    }

    // método Inserir videos Novo
    function inserirVideo($video){
        // chamar class basedados
        require_once "basedados.class.php";
        // saber a última notícia
        $ultima = $this->ultimoVideo();
        $ultimaID = (int)$ultima["id_video"] + 1;
        
        // importar a class de upload
        require_once "uploads.class.php";
        // fazer upload
        $upload = new Uploads();
        //$caminhoimg = $upload->uploadImg($video["imagem"],$ultimaID);
    
        $caminhovid = $upload->uploadVid($video["video"],$ultimaID);
        $caminhoimg = $upload->uploadImg($video["thumbnail"],$ultimaID."_img");
       
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        INSERT INTO VIDEOS 
        (id_categoria,id_utilizador,titulo,descricao,thumbnail,video,ativo)
        VALUES (:i,:iu,:t,:de,:th,:vid,:a)
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // colocar os parametros
        $dados->bindValue(':i',$video["id_categoria"]);
        $video["id_utilizador"] = $_SESSION["id"];
        $dados->bindValue(':iu',$video["id_utilizador"]);
        $dados->bindValue(':t',$video["titulo"]);
        $dados->bindValue(':de',$video["descricao"]);
        // data check format
        
       
        $dados->bindValue(':th',$caminhoimg);
        $dados->bindValue(':vid',$caminhovid);

        // check da checkbox
        $video["ativo"] = ($video["ativo"]=="on") ? true : false;
        $dados->bindValue(':a',$video["ativo"]);
        // executar query
        $dados->execute();
        
        // reencaminhar
        header("Location:privado.php");
    }
    
    // método para todas os videos
    function todosVideos($id){
            // vai buscar os videos
            // chamar class basedados
            require_once "basedados.class.php";
            // instanciar a classe
            
            $pdo = new BaseDados();
            $pdo = $pdo->getCon();
            if ($id == 1) {
                $sql = "
                SELECT VIDEOS.id_video, VIDEOS.id_categoria, VIDEOS.id_utilizador, UTILIZADORES.utilizador, CATEGORIAS.nomecat, VIDEOS.titulo, VIDEOS.descricao, VIDEOS.data, VIDEOS.thumbnail, VIDEOS.video, VIDEOS.ativo
                FROM CATEGORIAS,VIDEOS,UTILIZADORES
                WHERE CATEGORIAS.id_categoria = VIDEOS.id_categoria AND VIDEOS.ativo = true AND CATEGORIAS.ativo = true AND UTILIZADORES.id_utilizador = VIDEOS.id_utilizador
                ORDER BY VIDEOS.data DESC";
            }else{
                $sql = "
            SELECT VIDEOS.id_video, VIDEOS.id_categoria, VIDEOS.id_utilizador, UTILIZADORES.utilizador, CATEGORIAS.nomecat, VIDEOS.titulo, VIDEOS.descricao, VIDEOS.data, VIDEOS.thumbnail, VIDEOS.video, VIDEOS.ativo
            FROM CATEGORIAS,VIDEOS,UTILIZADORES
            WHERE CATEGORIAS.id_categoria = VIDEOS.id_categoria AND VIDEOS.ativo = true AND CATEGORIAS.ativo = true AND UTILIZADORES.id_utilizador = VIDEOS.id_utilizador AND VIDEOS.id_utilizador = :id
            ORDER BY VIDEOS.data DESC";
            }
           
            
            // prepara a ligação ao sql
            $dados = $pdo->prepare($sql);
            // bind values
            $dados -> bindValue(':id',$id);
            // executar query
            $dados->execute();
            // retornar valores
            return $resultado = $dados->fetchAll();
        }

    // método apagar videos por id
    function apagarVideos($id){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        DELETE FROM VIDEOS
        WHERE id_video = :i
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        $dados->bindValue(":i",$id);
        // executar query
        $dados->execute();
        // reencaminhar
        header("Location:vid_manager.php");
    }

    // método Editar videos 
    function editarVideo($video){
        // chamar class basedados
        require_once "basedados.class.php";
        // importar a class de upload
        require_once "uploads.class.php";
        // fazer upload
        $upload = new Uploads();
        // teste se é para editar imagem
        //getting get
        
        if($_FILES[$video["thumbnail"]]["size"]>0){
            // altera imagem
            // upload
            $caminho = $upload->uploadImg($video["thumbnail"],$video["id_video"]);
            // sql
            $sql = "
            UPDATE VIDEOS
            SET titulo = :t, descricao = :de, ativo = :a, id_categoria = :ic, thumbnail = :th
            WHERE id_video = :i
        ";    
        }else{
            // sem alterar imagem
            $sql = "
            UPDATE VIDEOS
            SET titulo = :t, descricao = :de, ativo = :a, id_categoria = :ic
            WHERE id_video = :i";
        }
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
  
        // trocar o checkbox = on
        if($video["ativo"]=="on"){
            $video["ativo"]=true;
        }else{
            $video["ativo"]=false;
        }
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // colocar os parametros
        $dados->bindValue(':t',$video["titulo"]);
        $dados->bindValue(':de',$video["descricao"]);
        $dados->bindValue(':a',$video["ativo"]);
        $dados->bindValue(':ic',$video["id_categoria"]);
        $dados->bindValue(':i',$video["id_video"]);
        // se muda imagem
        if ($_FILES[$video["thumbnail"]]["size"]>0) {$dados->bindValue(':th',$caminho);} 
        // executar query
        $dados->execute();

        var_dump($video);
        // reencaminhar
        header("Location:vid_manager.php");
        
    }


// fim da classe
}
?>