<?php
// class responsável pelos utilizadores

class Utilizadores {

    // construtor da class
    public function __construct(){
        
    }

    // método LOGIN
    function login($user, $password, $destino){
        require_once "./includes/config.inc.php";
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        SELECT id_utilizador, id_tipo_utilizador, utilizador, palavrapasse, email 
        FROM UTILIZADORES 
        WHERE ativo = true AND utilizador like :u AND palavrapasse like :p ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // prepara os parâmetros para incluir no sql
        $dados->bindValue(':u',$user);
        // MD5+SALT
        $newpassword = md5(SALT . SALT . SALT . $password . SALT);
        $dados->bindValue(':p',$newpassword);
        // executar query
        $dados->execute();
        // retornar valores
        $resultado = $dados->fetch();
        // se existir resultado que venha da base de dados então entra
        if ($resultado){
            // criar uma sessão de utilizador registado
            // inicializar a sessão, obrigatório para usar $_SESSION
            session_start();
            $_SESSION["login"] = true;
            $_SESSION["utilizador"]  = $resultado["utilizador"];
            $_SESSION["id"]    = $resultado["id_utilizador"];
            $_SESSION["tipo"] =  $resultado["id_tipo_utilizador"];
            // saltar para a página desejada
            header("Location:$destino");
        }else{
            // não entra - vai emitir uma mensagem
            $erro = <<< EOT
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>ERRO DE LOGIN</strong> Utilizador ou Palavra Passe estão erradas !!!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
EOT;
            echo $erro;
        }
        
    }

    // método logout
    function logout($destino){
        // reset às sessions
        session_start();
        // igualar a $_SESSION para um array vazio
        $_SESSION[] = array();
        // destroi a sessão no servidor
        session_destroy();
        // orientar a página de login
        header("Location:$destino");
    }

    // método secure - protege as páginas 
    // para acesso de utilizadores registados
    function secure(){
        // protecção anti-backdoor
        session_start();
        // detectar se o utilizador está registado
        if (!isset($_SESSION["login"])){
            // não tem acesso
            header("Location:login.php?erro=acesso");
        }
    }

    // método Lista de Tipos de Utilizadores
    function listaTiposUtilizadores(){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        SELECT id_tipo_utilizador, nome_tipo
        FROM TIPOS_UTILIZADORES
        WHERE ativo = true ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // executar query
        $dados->execute();
        // retornar valores
        return $resultado = $dados->fetchAll();
    }

    // método Inserir Utilizador Novo
    function inserirNovoUtilizador($tipo,$user,$pass,$nome,$email){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        INSERT INTO UTILIZADORES 
        (id_tipo_utilizador,utilizador,palavrapasse,email)
        VALUES (:id,:u,:p,:e)
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // colocar os parametros
        $dados->bindValue(':id',$tipo);
        $dados->bindValue(':u',$user);
        // MD5+SALT
        $newpassword = md5(SALT . SALT . SALT . $pass . SALT);
        $dados->bindValue(':p',$newpassword);
        $dados->bindValue(':e',$email);
        // executar query
        $dados->execute();
        // reencaminhar
        session_start();
        if(isset($_SESSION["login"])){
            header("Location:privado.php");
        } else {
            header("Location:login.php");
        }
        
    }
    
    // método Lista de Todos os Utilizadores
    function listaUtilizadores(){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        SELECT UTILIZADORES.id_utilizador, TIPOS_UTILIZADORES.nome_tipo , UTILIZADORES.utilizador, UTILIZADORES.email , UTILIZADORES.ativo
        FROM UTILIZADORES, TIPOS_UTILIZADORES
        WHERE UTILIZADORES.id_tipo_utilizador = TIPOS_UTILIZADORES.id_tipo_utilizador 
        ORDER BY UTILIZADORES.utilizador
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // executar query
        $dados->execute();
        // retornar valores
        return $resultado = $dados->fetchAll();
    }

    // método Lista de utilizador por id
    function utilizadorPorId($id){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        SELECT UTILIZADORES.id_utilizador, TIPOS_UTILIZADORES.nome_tipo , UTILIZADORES.utilizador, UTILIZADORES.email , UTILIZADORES.ativo, UTILIZADORES.id_tipo_utilizador
        FROM UTILIZADORES, TIPOS_UTILIZADORES
        WHERE UTILIZADORES.id_tipo_utilizador = TIPOS_UTILIZADORES.id_tipo_utilizador AND UTILIZADORES.id_utilizador = :i
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        $dados->bindValue(':i',$id);
        // executar query
        $dados->execute();
        // retornar valores
        return $resultado = $dados->fetch();
    }

    // método Inserir Utilizador Novo
    function editarUtilizador($util){
        // chamar class basedados
        require_once "basedados.class.php";
        
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        if($util["palavrapasse"] != ""){
            // vai alterar a palavra passe
            $sql = "
            UPDATE UTILIZADORES
            SET email = :e, ativo = :a, palavrapasse = :p, id_tipo_utilizador = :it
            WHERE id_utilizador = :i
            ";
        }else{
            // não altera a palavra passe
            $sql = "
            UPDATE UTILIZADORES
            SET email = :e, ativo = :a, id_tipo_utilizador = :it
            WHERE id_utilizador = :i
            ";
        }
        // trocar o checkbox = on
        if($util["ativo"]=="on"){
            $util["ativo"]=true;
        }else{
            $util["ativo"]=false;
        }
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        // colocar os parametros
        $dados->bindValue(':e',$util["email"]);
        $dados->bindValue(':a',$util["ativo"]);
        $dados->bindValue(':it',$util["id_tipo_utilizador"]);
        $dados->bindValue(':i',$util["id_utilizador"]);
        if ($util["palavrapasse"]!=""){
            // MD5+SALT
            $newpassword = md5(SALT . SALT . SALT . $util["palavrapasse"] . SALT);
            $dados->bindValue(':p',$newpassword);
        }
        // executar query
        $dados->execute();
        // reencaminhar
        header("Location:user_manager.php");
    }
    
    // método apagar Utilizadores por id
    function apagarUtilizador($id){
        // chamar class basedados
        require_once "basedados.class.php";
        // instanciar a classe
        $pdo = new BaseDados();
        $pdo = $pdo->getCon();
        $sql = "
        DELETE FROM UTILIZADORES
        WHERE id_utilizador = :i
        ";
        // prepara a ligação ao sql
        $dados = $pdo->prepare($sql);
        $dados->bindValue(":i",$id);
        // executar query
        $dados->execute();
        // reencaminhar
        header("Location:user_manager.php");
    }

    

// fim de classe
}
?>