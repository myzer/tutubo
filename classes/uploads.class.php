<?php
// class responsável pelo upload
class Uploads{
    // construtor da class
    public function __construct (){
    }
    // função para inserir video 
    // $videos é o nome do campo do formulário
    // $nome é o nome da pasta a criar
    function uploadVid($videos,$nome){
        // path directorio principal
        $currentDir = getcwd();
        // path para onde vai os uploads
        $uploadDirectory = 'uploads/vid';
        $errors = []; // guarda os erros
        $fileExtensions = ['mp4','MP4']; // extensões permitidas
        // nome do ficheiro com a extensão
        $fileName = $_FILES[$videos]['name'];
        // tamanho do ficheiro
        $fileSize = $_FILES[$videos]['size'];
        // nome do ficheiro temporario
        $fileTmpName  = $_FILES[$videos]['tmp_name'];
        // tipo de ficheiro
        $fileType = $_FILES[$videos]['type'];
        // extensão do ficheiro que vai ser upload
        $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
        // caminho completo com o ficheiro + extensão
        $uploadPath =  $currentDir . '/' . $uploadDirectory . '/' .$nome . '/'. basename($fileName); 
        // caminho apenas
        $path = $currentDir . '/'. $uploadDirectory . '/' .$nome;
        // check se existe directorio
        if (file_exists($path)) {
            // existe pasta
            // verificar se existe ficheiro com o mesmo nome
            if (file_exists($uploadPath)){
                $newFileName = uniqid() . "." .  $fileExtension; 
                $fileName = $newFileName;
                $uploadPath = $currentDir . '/'. $uploadDirectory . '/' .$nome . '/'. basename($fileName); 
            }
        } else {
        // não existe
            // cria pasta
            // windows ignora o 2 parametro (linux e mac)
            mkdir($path, 0757);
        }
        // check se a extensão não é permitida
        if (!in_array($fileExtension,$fileExtensions)) {
            $errors[] = "Extensão de ficheiro não permitida. Por favor use JPEG ou PNG";
        }
        // check tamanho de ficheiro
        if ($fileSize > 200000000) {
            $errors[] = "Este ficheiro ultrapassa 2MB. Tem que introduzir uma imagem mais pequena. ";
        }
        // senão existir erros
        if (empty($errors)) {
            // faz upload
            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
            if ($didUpload) {
                // echo "O ficheiro " . basename($fileName) . " foi gravado com sucesso";
                $dbpath = $uploadDirectory . '/' .$nome . '/'. basename($fileName);
                return $dbpath;
            } else {
                // echo "Aconteceu um erro ao gravar o ficheiro"; 

            }
        } else {
            // lista erros
            foreach ($errors as $error) {
                echo $error . "Lista de Erros" . "\n";
            }
        }
    }

     // função para inserir video 
    // $videos é o nome do campo do formulário
    // $nome é o nome da pasta a criar

    function uploadImg($picture,$nome){
        // path directorio principal
        $currentDir = getcwd();
        // path para onde vai os uploads
        $uploadDirectory = 'uploads/img';
        $errors = []; // guarda os erros
        $fileExtensions = ['jpg','jpeg','png','JPG','JPEG','PNG']; // extensões permitidas
        // nome do ficheiro com a extensão
        $fileName = $_FILES[$picture]['name'];
        // tamanho do ficheiro
        $fileSize = $_FILES[$picture]['size'];
        // nome do ficheiro temporario
        $fileTmpName  = $_FILES[$picture]['tmp_name'];
        // tipo de ficheiro
        $fileType = $_FILES[$picture]['type'];
        // extensão do ficheiro que vai ser upload
        $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
        // caminho completo com o ficheiro + extensão
        $uploadPath =  $currentDir . '/' . $uploadDirectory . '/' .$nome . '/'. basename($fileName); 
        // caminho apenas
        $path = $currentDir . '/'. $uploadDirectory . '/' .$nome;
        // check se existe directorio
        if (file_exists($path)) {
            // existe pasta
            // verificar se existe ficheiro com o mesmo nome
            if (file_exists($uploadPath)){
                $newFileName = uniqid() . "." .  $fileExtension; 
                $fileName = $newFileName;
                $uploadPath = $currentDir . '/'. $uploadDirectory . '/' .$nome . '/'. basename($fileName); 
            }
        } else {
        // não existe
            // cria pasta
            // windows ignora o 2 parametro (linux e mac)
            mkdir($path, 0757);
        }
        // check se a extensão não é permitida
        if (!in_array($fileExtension,$fileExtensions)) {
            $errors[] = "Extensão de ficheiro não permitida. Por favor use JPEG ou PNG";
        }
        // check tamanho de ficheiro
        if ($fileSize > 20485760000) {
            $errors[] = "Este ficheiro ultrapassa 200MB. Tem que introduzir uma imagem mais pequena. ";
        }
        // senão existir erros
        if (empty($errors)) {
            // faz upload
            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
            if ($didUpload) {
                // echo "O ficheiro " . basename($fileName) . " foi gravado com sucesso";
                $dbpath = $uploadDirectory . '/' .$nome . '/'. basename($fileName);
                return $dbpath;
            } else {
                // echo "Aconteceu um erro ao gravar o ficheiro"; 

            }
        } else {
            // lista erros
            foreach ($errors as $error) {
                echo $error . "Lista de Erros" . "\n";
            }
        }
    }
   
}
?>
