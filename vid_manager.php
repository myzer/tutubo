<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Vídeos \ 
          <small class="badge badge-dark menuativo">GESTÃO</small></h1>
          <hr>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Ativo</th>
                        <th>Categoria</th>
                        <th>Título</th>
                        <th>Uploader</th>
                        <th>Data</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    require_once "classes/videos.class.php";
                    $v = new Videos();
                    $videos = $v->todosVideos($_SESSION["tipo"]);
                    foreach($videos as $video){
                    ?>
                    <!-- linha tabela -->
                    <tr>
                        <td><?php 
                                   if ($video["ativo"]){
                                        echo '<i class="far fa-eye text-success" title="Video Ativo"></i>';
                                   }else{
                                        echo '<i class="far fa-eye-slash text-danger" title="Video Não Ativo"></i>';
                                   } 
                            ?>
                        </td>
                        <td><?php echo $video["nomecat"]; ?></td>
                        <td><?php echo $video["titulo"]; ?></td>
                        <td><?php echo $video["utilizador"]; ?></td>

                        <td><?php echo date('d-m-Y', strtotime($video["data"])); ?></td>
                        
                        <td>
                            <!-- botão opções -->
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="fas fa-cogs"></i> 
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="vid_edit.php?id=<?php echo $video["id_video"]; ?>">Editar</a>
                                    <a class="dropdown-item" href="#" onmousedown="confirmaApagar(<?php echo $video['id_video'];?>,'vid_delete.php?id=');">Apagar</a>                            
                                </div>
                            </div>
                            <!-- botão opções -->
                        </td>
                        
                    </tr>
                    <!-- fim de linha tabela -->
                    <?php } ?>
                </tbody>
            
            </table>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>