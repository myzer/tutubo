<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
  // detetar se vem uma querystring id no url
  if(isset($_GET["id"]) && is_numeric($_GET["id"])){
    $dadosatuais = $u->utilizadorPorId($_GET["id"]);
  }
  // detectar se passou o inserir
  if (isset($_POST["btnEditar"])){
    // editar o utilizador
    // chamar o método para editar
    $u->editarUtilizador($_POST);
  }
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Utilizadores \ 
          <small class="badge badge-dark menuativo">EDITAR</small></h1>
          <hr>
          <button type="button" class="btn btn-dark btn-sm" onmousedown="javascript:history.back();"> < voltar</button>
          <hr>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <form action="user_edit.php" method="post" onsubmit="return validaUtilizadorEditar();">
            <div class="form-group">
              <label for="id_tipo_utilizador">Defina o tipo de utilizador</label>
              <select id="id_tipo_utilizador" name="id_tipo_utilizador" class="form-control">
                <option value="0">--- Escolha um tipo</option>
                <?php 
                  $tipos = $u->listaTiposUtilizadores();
                  foreach ($tipos as $tipo){
                ?>
                  <option value="<?php echo $tipo["id_tipo_utilizador"]; ?>" <?php 
                  if ($tipo["id_tipo_utilizador"]==$dadosatuais["id_tipo_utilizador"]){echo " selected";}
                  ?>><?php echo $tipo["nome_tipo"]; ?></option>
                <?php } ?>
              </select>
            </div>
            
            <div class="form-group">
              <label for="utilizador">Utilizador</label>
              <input type="text" id="utilizador" name="utilizador" class="form-control" value="<?php echo $dadosatuais['utilizador'];?>" readonly>
            </div>

            <div class="form-group">
              <label for="nome">Nome do Utilizador</label>
            </div> 

            <div class="form-group">
              <label for="email">Email do Utilizador</label>
              <input type="email" id="email" name="email" class="form-control" value="<?php echo $dadosatuais['email'];?>">
            </div>  

            <div class="form-group">
              <label for="palavrapasse">Digite a Palavra Passe</label>
              <input type="password" id="palavrapasse" name="palavrapasse" class="form-control" >
            </div>

            <div class="form-group">
              <label for="palavrapasse2">Confirme a Palavra Passe</label>
              <input type="password" id="palavrapasse2" name="palavrapasse2" class="form-control">
            </div>

            

            <div class="form-group">
            <input type="checkbox" id="ativo" name="ativo" <?php 
            if ($dadosatuais['ativo']){echo " checked";}
            ?>> <label for="ativo"> Utilizador Ativo</label>
                
            </div>  

            <hr>
            <input type="hidden" name="id_utilizador" value="<?php echo $_GET["id"];?>">
            <button type="submit" class="btn btn-success" name="btnEditar" value="editar">EDITAR</button>          <button type="reset" class="btn btn-danger">LIMPAR</button>             <hr>              

          </form>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>