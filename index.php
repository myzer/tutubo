<?php 
require_once "classes/videos.class.php";
$v = new Videos();
// método para ir buscar 3 notícias
$ultimos = $v->ultimosVideos(3);

?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head.inc.php';?>
  </head>
  <body>
    <?php require_once 'includes/menu.inc.php'; ?>
    
    <div class="container mt-3">
      <!-- titulo -->
      <div class="row">
        <div class="col-12">
          <div class="jumbotron" id="tituloprincipal">
            <h1 class="display-4 text-center text-white"><?php echo NOMEWEBSITE;?></h1>
          </div>
        </div>
      </div>
      <!-- fim de titulo -->

      <!-- zona dos ultimos videos -->
      <div class="row">
        <div class="col-12">
            <h1 class="display-5">Últimos Vídeos</h1><hr>
        </div>
        <?php 
        foreach($ultimos as $video){
        ?>
        <!-- cartão -->
        <div class="col-12 col-md-4">
            <div class="card">
               <a href="video.php?id=<?php echo $video['id_video'] ?>"> <img class="card-img-top" src="<?php echo $video['thumbnail']; ?>"></a>
                <div class="card-body">
                    <span class="badge badge-warning"><?php echo $video['nomecat']; ?></span>
                    <span class="badge badge-warning"><?php echo $video["utilizador"];?></span>
                    <h5 class="card-title"><?php echo $video['titulo']; ?></h5>
                    <p class="card-text"><?php 
                    $desc = $video['descricao'];
                    $desc = mb_strimwidth($desc,0,30,'...');
                    // limpar o html
                    $desc = strip_tags($desc);
                    echo $desc;
                    ?></p>
                    <small><?php echo date('d/m/Y',strtotime($video['data'])); ?></small>
                    <hr>
                    
                </div>
            </div>
        </div>
        <!-- fim de cartão -->

        <?php }?>
       
      </div>  
      <!-- fim de zona dos ultimos videos -->


    <!-- fim do container -->
    </div>
    
  <?php require_once 'includes/rodape.inc.php'; ?>  

  <?php require_once 'includes/scripts.inc.php';?>
  </body>
</html>