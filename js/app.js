// Javascript

// validação de formulário de login
function validaLogin(){
    let user = document.getElementById("utilizador").value;
    let password = document.getElementById("palavrapasse").value;
    if(user == "" || password == ""){
        // apresentar o erro
        let mensagem = document.createElement("div");
        mensagem.innerHTML = "<b>Utilizador</b> ou <b>Palavra Passe</b> obrigatórios";
        swal({
            title: "Erro de Login!",
            content: mensagem,
            icon: "error",
        });
        // erro
        return false;
    }else{
        // passou a validação
        return true;
    }   
}

// validar inserir utilizador novo
function validaUtilizadorNovo(){
    let tipo = document.getElementById("id_tipo_utilizador").value;
    let utilizador = document.getElementById("utilizador").value;
    let pass = document.getElementById("palavrapasse").value; 
    let pass2 = document.getElementById("palavrapasse2").value;
    
    let erros = "";
    if (tipo=="0"){ erros="Por favor escolha um <b>tipo de utilizador</b> válido!<br>"; }
    if (utilizador==""){ erros+="Por favor digite um <b>utilizador</b><br>"; }
    if (pass != ""){
        if (pass != pass2){ erros+="As <b>palavras passes</b> tem que ser iguais<br>"; }
    }else{
        erros+="A <b>palavra passe</b> não pode ser vazia!<br>";
    }
    // verificar erros
    if (erros != ""){
        // apresenta erro
        let mensagem = document.createElement("div");
        mensagem.innerHTML = erros;
        swal({
            title: "Formulário com erros",
            content: mensagem,
            icon: "error",
        });
        // erro
        return false;
    }else{
        // sem erros
        return true;
    }
}

// validar inserir utilizador na edição
function validaUtilizadorEditar(){
    let tipo = document.getElementById("id_tipo_utilizador").value;
    let utilizador = document.getElementById("utilizador").value;
    let pass = document.getElementById("palavrapasse").value; 
    let pass2 = document.getElementById("palavrapasse2").value;
    let erros = "";
    if (tipo=="0"){ erros="Por favor escolha um <b>tipo de utilizador</b> válido!<br>"; }
    if (utilizador==""){ erros+="Por favor digite um <b>utilizador</b><br>"; }
    if (pass.length > 0){
        if (pass != pass2){ erros+="As <b>palavras passes</b> tem que ser iguais<br>"; }
    }
    // verificar erros
    if (erros != ""){
        // apresenta erro
        let mensagem = document.createElement("div");
        mensagem.innerHTML = erros;
        swal({
            title: "Formulário com erros",
            content: mensagem,
            icon: "error",
        });
        // erro
        return false;
    }else{
        // sem erros
        return true;
    }
}


// função confirma apagar
function confirmaApagar(id,caminho){
    swal({
        title: "Tem a certeza ?",
        text: "O seu registo será apagado da base de dados",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          // apagar
          window.location.href = caminho + id;
        } 
      });
}