<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
  require_once "classes/categorias.class.php";
  $cat = new Categorias();
  // detetar se vem uma querystring id no url
  if(isset($_GET["id"]) && is_numeric($_GET["id"])){
    $dadosatuais = $cat->categoriaPorId($_GET["id"]);
  }
  // detectar se passou o inserir
  if (isset($_POST["btnEditar"])){
    // editar o utilizador
    // chamar o método para editar
    $cat->editarCategoria($_POST);
  }
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Categorias \ 
          <small class="badge badge-dark menuativo">EDITAR</small></h1>
          <hr>
          <button type="button" class="btn btn-dark btn-sm" onmousedown="javascript:history.back();"> < voltar</button>
          <hr>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <form action="cat_edit.php" method="post" onsubmit="">
            
            
            <div class="form-group">
              <label for="nomecat">Nome da Categoria</label>
              <input type="text" id="nomecat" name="nomecat" class="form-control" value="<?php echo $dadosatuais['nomecat'];?>">
            </div>

            <div class="form-group">
                <input type="checkbox" id="ativo" name="ativo" <?php 
                if ($dadosatuais['ativo']){echo " checked";}
                ?>> <label for="ativo"> Categoria Ativa</label>
            </div>  

            <hr>
            <input type="hidden" name="id_categoria" value="<?php echo $_GET["id"];?>">
            <button type="submit" class="btn btn-success" name="btnEditar" value="editar">EDITAR</button>          <button type="reset" class="btn btn-danger">LIMPAR</button>             <hr>              

          </form>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>