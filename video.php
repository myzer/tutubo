<?php 
// filtro do número da noticia
if(isset($_GET["id"]) && is_numeric($_GET["id"])){
    $filtroid = $_GET["id"];
}
require_once "classes/videos.class.php";
$v = new Videos();
// chamar o método notícia por ID
$video = $v->videosPorId($filtroid);
// chamar método últimas noticias
$ultimas = $v->ultimosVideos(5);
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head.inc.php';?>
  </head>
  <body>
    <?php require_once 'includes/menu.inc.php'; ?>
    
    <div class="container mt-3">
      <!-- titulo -->
      <div class="row">
        <div class="col-12">
          <div class="jumbotron" id="tituloprincipal">
            <h1 class="display-4 text-center"><?php echo NOMEWEBSITE;?></h1>
          </div>
        </div>
      </div>
      <!-- fim de titulo -->

      <!-- noticias -->
      <div class="row">
        <div class="col-12">
            <span class="badge badge-warning"><?php echo $video["nomecat"];?></span>
            <span class="badge badge-warning"><?php echo $video["utilizador"];?></span>
            <h1 class="display-5"><?php echo $video["titulo"];?></h1><hr>
        </div>
      </div>  
      <div class="row">
          <!-- noticia -->
          <article class="col-12 col-md-8 mb-3">
            <video width="640" height="480" controls>
                <source src="<?php echo $video["video"]; ?>" type="video/mp4">  
            </video>
              <p>
              <?php echo $video["descricao"];?>
              <br>
              <small class="mt-4"><?php echo date('d/m/Y',strtotime($video['data']));?></small>
              </p>

                <hr>
                <?php 
                if (isset($_GET["string"])){
                    echo '<a href="pesquisa.php?pesquisa='.$_GET["string"].'"><button type="button" class="btn btn-sm btn-warning"> < Voltar</button></a>';
                }else{
                    echo '<button type="button" class="btn btn-sm btn-warning" onmousedown="javascript:history.back();"> < Voltar</button>';
                }
                ?>
               
          </article>
          <!-- fim de noticia -->
          <!-- lateral -->
          <aside class="col-12 col-md-4">
          <?php 
          foreach ($ultimas as $ultima){
          ?>
             <!-- cartao lateral --> 
             <div class="card mb-3">
                <img class="card-img-top" src="<?php echo $ultima["thumbnail"];?>" >
                <div class="card-body">
                    <a href="video.php?id=<?php echo $ultima["id_video"];?>"><p class="card-text"><?php echo $ultima["titulo"];?></p></a>
                </div>
            </div>
            <!-- fim de cartao lateral -->
          <?php }?>                            
          </aside>
          <!-- fim de lateral -->
      </div>
      <!-- fim de notícias -->


    <!-- fim do container -->
    </div>
    
  <?php require_once 'includes/rodape.inc.php'; ?>  

  <?php require_once 'includes/scripts.inc.php';?>
  </body>
</html>