<?php 
require_once "classes/videos.class.php";
$v = new Videos();
// pesquisar noticias
$string = $_REQUEST["pesquisa"];
$resultados = $v->pesquisarVideo($string);
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head.inc.php';?>
  </head>
  <body>
    <?php require_once 'includes/menu.inc.php'; ?>
    
    <div class="container mt-3">
      <!-- titulo -->
      <div class="row">
        <div class="col-12">
          <div class="jumbotron" id="tituloprincipal">
            <h1 class="display-4 text-center text-white"><?php echo NOMEWEBSITE;?></h1>
          </div>
        </div>
      </div>
      <!-- fim de titulo -->

      <!-- zona das pesquisa -->
      <div class="row">
        <div class="col-12">
            <h1 class="display-5">Pesquisar por <em class="badge badge-success"><?php echo $string;?></em></h1><hr>
            <?php 
            foreach ($resultados as $resultado){
            ?>
            <!-- linha de pesquisa -->
            <div class="mb-3">
                <a href="video.php?id=<?php echo $resultado["id_video"]; ?>&string=<?php echo $string?>"><h6><?php echo $resultado["titulo"]; ?></h6></a>
                <p class="mb-0">
                <?php 
                $sinopse = mb_strimwidth($resultado["descricao"],0,50);
                echo strip_tags($sinopse);
                ?>
                </p>
                <small><em><?php echo date('d/m/Y',strtotime($resultado['data'])); ?></em></small>
            </div>
            <!-- fim de linha de pesquisa -->
            <?php 
            } //fim foreach
            //apresentar não foram encontrados resultados
            if (empty($resultados)){
              require_once "classes/mensagens.class.php";
              $m = new Mensagens();
              // mostra mensagem
              //tipo = bootstrap
              //tipo pode ser: info, warning, danger, primary, success

              $m->mostraMensagem('Não foram encontrados resultados', 'Tente pesquisar por outra(s) palavras chave', 'info');
              
            } ?>
        </div>
      </div>
      <!-- fim zona das pesquisa -->  


    <!-- fim do container -->
    </div>
    
  <?php require_once 'includes/rodape.inc.php'; ?>  

  <?php require_once 'includes/scripts.inc.php';?>
  </body>
</html>