-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2018 at 03:05 PM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tutubo`
--
CREATE DATABASE IF NOT EXISTS `tutubo` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `tutubo`;

-- --------------------------------------------------------

--
-- Table structure for table `CATEGORIAS`
--

DROP TABLE IF EXISTS `CATEGORIAS`;
CREATE TABLE IF NOT EXISTS `CATEGORIAS` (
  `id_categoria` int(11) NOT NULL,
  `nomecat` varchar(64) NOT NULL,
  `criadoem` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `CATEGORIAS`
--

INSERT INTO `CATEGORIAS` (`id_categoria`, `nomecat`, `criadoem`, `ativo`) VALUES
(1, 'Música', '2018-11-11 19:27:53', 1),
(3, 'Gameplays', '2018-11-11 19:28:21', 1),
(4, 'Vlogs', '2018-11-20 14:39:56', 1),
(5, 'Cursos', '2018-11-22 14:46:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `TIPOS_UTILIZADORES`
--

DROP TABLE IF EXISTS `TIPOS_UTILIZADORES`;
CREATE TABLE IF NOT EXISTS `TIPOS_UTILIZADORES` (
  `id_tipo_utilizador` int(11) NOT NULL,
  `nome_tipo` varchar(32) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `criadoem` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `TIPOS_UTILIZADORES`
--

INSERT INTO `TIPOS_UTILIZADORES` (`id_tipo_utilizador`, `nome_tipo`, `ativo`, `criadoem`) VALUES
(1, 'Administrador', 1, '2018-11-11 19:25:46'),
(2, 'Utilizador Normal', 1, '2018-11-11 19:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `UTILIZADORES`
--

DROP TABLE IF EXISTS `UTILIZADORES`;
CREATE TABLE IF NOT EXISTS `UTILIZADORES` (
  `id_utilizador` int(11) NOT NULL,
  `id_tipo_utilizador` int(11) NOT NULL,
  `utilizador` varchar(32) NOT NULL,
  `palavrapasse` varchar(32) NOT NULL,
  `email` varchar(120) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `criadoem` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `UTILIZADORES`
--

INSERT INTO `UTILIZADORES` (`id_utilizador`, `id_tipo_utilizador`, `utilizador`, `palavrapasse`, `email`, `ativo`, `criadoem`) VALUES
(1, 1, 'admin', '605f76aaaa615794932d6dcf67e35152', 'admin@admin.com', 1, '2018-11-11 19:27:08'),
(2, 2, 'MyzerHD', 'ff44dc524a2587ede0bb30d931135855', 'marcio@gmail.com', 1, '2018-11-11 19:27:39');

-- --------------------------------------------------------

--
-- Table structure for table `VIDEOS`
--

DROP TABLE IF EXISTS `VIDEOS`;
CREATE TABLE IF NOT EXISTS `VIDEOS` (
  `id_video` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_utilizador` int(11) NOT NULL,
  `titulo` varchar(256) NOT NULL,
  `descricao` longtext NOT NULL,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `thumbnail` varchar(256) NOT NULL,
  `video` varchar(256) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `VIDEOS`
--

INSERT INTO `VIDEOS` (`id_video`, `id_categoria`, `id_utilizador`, `titulo`, `descricao`, `data`, `thumbnail`, `video`, `ativo`) VALUES
(11, 1, 2, 'Luis Fonsi - Gritar', 'Vídeo com letra da música "Gritar" de Luis Fonsi. Divirtam-se! ', '2018-11-22 14:59:47', 'uploads/img/1_img/fonsi.jpg', 'uploads/vid/1/fonsi.mp4', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CATEGORIAS`
--
ALTER TABLE `CATEGORIAS`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `TIPOS_UTILIZADORES`
--
ALTER TABLE `TIPOS_UTILIZADORES`
  ADD PRIMARY KEY (`id_tipo_utilizador`);

--
-- Indexes for table `UTILIZADORES`
--
ALTER TABLE `UTILIZADORES`
  ADD PRIMARY KEY (`id_utilizador`),
  ADD KEY `FK_TIPOS_UTILIZADORES_UTILIZADORES` (`id_tipo_utilizador`) USING BTREE;

--
-- Indexes for table `VIDEOS`
--
ALTER TABLE `VIDEOS`
  ADD PRIMARY KEY (`id_video`),
  ADD KEY `FK_UTILIZADORES_VIDEOS` (`id_utilizador`),
  ADD KEY `FK_CATEGORIAS_VIDEOS` (`id_categoria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `CATEGORIAS`
--
ALTER TABLE `CATEGORIAS`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `TIPOS_UTILIZADORES`
--
ALTER TABLE `TIPOS_UTILIZADORES`
  MODIFY `id_tipo_utilizador` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `UTILIZADORES`
--
ALTER TABLE `UTILIZADORES`
  MODIFY `id_utilizador` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `VIDEOS`
--
ALTER TABLE `VIDEOS`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `UTILIZADORES`
--
ALTER TABLE `UTILIZADORES`
  ADD CONSTRAINT `utilizadores_ibfk_1` FOREIGN KEY (`id_tipo_utilizador`) REFERENCES `TIPOS_UTILIZADORES` (`id_tipo_utilizador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `VIDEOS`
--
ALTER TABLE `VIDEOS`
  ADD CONSTRAINT `videos_ibfk_1` FOREIGN KEY (`id_utilizador`) REFERENCES `UTILIZADORES` (`id_utilizador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `videos_ibfk_2` FOREIGN KEY (`id_categoria`) REFERENCES `CATEGORIAS` (`id_categoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
