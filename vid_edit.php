<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
  require_once "classes/videos.class.php";
  // detetar se vem uma querystring id no url
  if(isset($_GET["id"]) && is_numeric($_GET["id"])){
   
    $v = new Videos();
    $dadosatuais = $v->videosPorId($_GET["id"]);
  }
  // detectar se passou o editar
  if (isset($_POST["btnEditar"])){
    // editar video
    $vid = new Videos();
    $vid ->editarVideo($_POST);
    
  }
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Videos \ 
          <small class="badge badge-dark menuativo">EDITAR</small></h1>
          <hr>
          <button type="button" class="btn btn-dark btn-sm" onmousedown="javascript:history.back();"> < voltar</button>
          <hr>
        </div>
      </div>
 
      <div class="row">
        <div class="col-12">
          <form action="vid_edit.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
              <label for="id_categoria">Escolha a categoria do Video</label>
              <select id="id_categoria" name="id_categoria" class="form-control">
                <option value="0">--- Escolha uma categoria</option>
                <?php 
                require_once "classes/categorias.class.php";
                $c = new Categorias();
                $categorias = $c->listaCategorias();
                  foreach ($categorias as $categoria){
                ?>
                  <option value="<?php echo $categoria["id_categoria"]; ?>"><?php echo $categoria["nomecat"]; ?></option>
                <?php } ?>
              </select>
            </div>
             
            <div class="form-group">
              <label for="titulo">Título do Video</label>
              <input type="text" id="titulo" name="titulo" class="form-control" value="<?php echo htmlentities($dadosatuais["titulo"]);?>">
            </div>
 
            <div class="form-group">
              <label for="descricao">Descrição do Video</label>
              <textarea rows="10" cols="60" name="descricao" id="descricao" class="form-control"<?php echo $dadosatuais["descricao"]; ?> ></textarea>
            </div>
 
            
 
            <div class="form-group">
              <label for="imagem">Thumbnail</label>
              <input type="file" id="imgupload" name="imgupload" class="form-control" value="<?php echo $dadosatuais["thumbnail"];?>">
            </div> 
 
            <div class="form-group">
                <input type="checkbox" id="ativo" name="ativo"  checked> <label for="ativo">Video Ativo</label>
            </div>  
 
            <hr>
            <input type="hidden" id="thumbnail" name="thumbnail" value="imgupload">
            <input type="hidden" id="id_video" name="id_video" value="<?php echo $_GET["id"] ?>">

            <button type="submit" class="btn btn-success" name="btnEditar" value="editar">EDITAR</button>          <button type="reset" class="btn btn-danger">LIMPAR</button>             <hr>              
 
          </form>
        </div>
      </div>
       
   
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
   
    
    
  </body>
</html>