<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Utilizadores \ 
          <small class="badge badge-dark menuativo">GESTÃO</small></h1>
          <hr>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Ativo</th>
                        <th>Tipo</th>
                        <th>Utilizador</th>
                        <th>Email</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $utilizadores = $u->listaUtilizadores();
                    foreach($utilizadores as $util){
                    ?>
                    <!-- linha tabela -->
                    <tr>
                        <td><?php 
                                   if ($util["ativo"]){
                                        echo '<i class="far fa-eye text-success" title="Utilizador Ativo"></i>';
                                   }else{
                                        echo '<i class="far fa-eye-slash text-danger" title="Utilizador Não Ativo"></i>';
                                   } 
                            ?>
                        </td>
                        <td><?php echo $util["nome_tipo"]; ?></td>
                        <td><?php echo $util["utilizador"]; ?></td>
                        <td><?php echo $util["email"]; ?></td>
                        <td>
                            <!-- botão opções -->
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="fas fa-cogs"></i> 
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="user_edit.php?id=<?php echo $util["id_utilizador"]; ?>">Editar</a>
                                    <a class="dropdown-item" href="#" onmousedown="confirmaApagar(<?php echo $util['id_utilizador'];?>, 'user_delete.php?id=');">Apagar</a>                            
                                </div>
                            </div>
                            <!-- botão opções -->
                        </td>
                    </tr>
                    <!-- fim de linha tabela -->
                    <?php } ?>
                </tbody>
            
            </table>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>