<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Categorias \ 
          <small class="badge badge-dark menuativo">GESTÃO</small></h1>
          <hr>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
            <table class="table table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Ativa</th>
                        <th>Nome Catergoria</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    require_once "classes/categorias.class.php";
                    $cat = new Categorias();
                    $categorias = $cat->listaCategorias(true);
                    foreach($categorias as $categoria){
                    ?>
                    <!-- linha tabela -->
                    <tr>
                        <td><?php 
                                   if ($categoria["ativo"]){
                                        echo '<i class="far fa-eye text-success" title="Utilizador Ativo"></i>';
                                   }else{
                                        echo '<i class="far fa-eye-slash text-danger" title="Utilizador Não Ativo"></i>';
                                   } 
                            ?>
                        </td>
                        <td><?php echo $categoria["nomecat"]; ?></td>
                        <td>
                            <!-- botão opções -->
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                 <i class="fas fa-cogs"></i> 
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="cat_edit.php?id=<?php echo $categoria["id_categoria"]; ?>">Editar</a>
                                    <a class="dropdown-item" href="#" onmousedown="confirmaApagar(<?php echo $categoria['id_categoria'];?>,'cat_delete.php?id=');">Apagar</a>                            
                                </div>
                            </div>
                            <!-- botão opções -->
                        </td>
                    </tr>
                    <!-- fim de linha tabela -->
                    <?php } ?>
                </tbody>
            
            </table>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>