<?php 
require_once "classes/videos.class.php";
$v = new Videos();
// verificar se existe filtro cat no url
if (isset($_GET["cat"]) && is_numeric($_GET["cat"])){
    $filtro_cat = $_GET["cat"];
}
// método para ir buscar todas os videos da categoria
$videos = $v->videosPorCat($filtro_cat);

?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head.inc.php';?>
  </head>
  <body>
    <?php require_once 'includes/menu.inc.php'; ?>
    
    <div class="container mt-3">


      <!-- zona videos -->
      <div class="row">
        <div class="col-12">
            <h1 class="display-5">
            <?php 
            require_once "classes/categorias.class.php";
            $c = new Categorias();
            $nomecategoria = $c->categoriaPorId($filtro_cat);
            echo $nomecategoria["nomecat"];
            ?>
            </h1><hr>
        </div>
        <?php 
        // testa se array vem com dados
        if (empty($videos)){
            echo "<h4>Neste momento não existem notícias para a categoria {$nomecategoria["nomecat"]}</h4>";
        }
        foreach($videos as $video){
        ?>
        <!-- cartão -->
        <div class="col-6 col-md-3 mb-4">
        <div class="card">
               <a href="video.php?id=<?php echo $video['id_video'] ?>"> <img class="card-img-top" src="<?php echo $video['thumbnail']; ?>"></a>
                <div class="card-body">
                    <span class="badge badge-warning"><?php echo $video['nomecat']; ?></span>
                    <span class="badge badge-warning"><?php echo $video["utilizador"];?></span>
                    <h5 class="card-title"><?php echo $video['titulo']; ?></h5>
                    <p class="card-text"><?php 
                    $desc = $video['descricao'];
                    $desc = mb_strimwidth($desc,0,30,'...');
                    // limpar o html
                    $desc = strip_tags($desc);
                    echo $desc;
                    ?></p>
                    <small><?php echo date('d/m/Y',strtotime($video['data'])); ?></small>
                    <hr>
                    
                </div>
            </div>
        </div>
        <!-- fim de cartão -->
 
        
        <?php }?>
       
      </div>  
      <!-- fim de zona de categorias-->


    <!-- fim do container -->
    </div>
    
  <?php require_once 'includes/rodape.inc.php'; ?>  

  <?php require_once 'includes/scripts.inc.php';?>
  </body>
</html>