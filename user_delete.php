<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
  // chamar o método que apaga o utilizador
  if(isset($_GET["id"]) && is_numeric($_GET["id"])){
      // apaga o user
      $u->apagarUtilizador($_GET["id"]);
  } else {
      header("Location:privado.php?erro=get");
  }

?>