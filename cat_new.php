<?php 
  // instanciar a class utilizadores
  require_once "classes/utilizadores.class.php";
  $u = new Utilizadores();
  // verificar antibackdoor
  $u->secure();
  // detectar se passou o inserir
  if (isset($_POST["btnInserir"])){
    // chama o metodo para inserir a categoria
    require "classes/categorias.class.php";
    $cat = new Categorias();
    $cat->inserirCategoria($_POST['nomecat']);
  }
?>
<!DOCTYPE html>
<html lang="pt-pt">
  <head>
    <?php require_once 'includes/head_backoffice.inc.php'; ?>
  </head>
  <body>
    <?php require_once 'includes/menu_backoffice.inc.php'; ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <h1>Categorias \ 
          <small class="badge badge-dark menuativo">INSERIR NOVA</small></h1>
          <hr>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <form action="cat_new.php" method="post" onsubmit="">
            
            
            <div class="form-group">
              <label for="nomecat">Nome da Nova Categoria</label>
              <input type="text" id="utilizador" name="nomecat" class="form-control" placeholder="Digite a sua nova categoria">
            </div>

            <hr>
            <button type="submit" class="btn btn-success" name="btnInserir" value="inserir">INSERIR</button>          <button type="reset" class="btn btn-danger">LIMPAR</button>             
            <hr>              
          </form>
        </div>
      </div>
      
  
    </div>
    <?php require_once 'includes/scripts_backoffice.inc.php';?>
  </body>
</html>